#! /usr/bin/env python3

from instance import instance
from views import *

#----------------------------------------------------------------------------#
# App Config.
#----------------------------------------------------------------------------#

#if not app.debug:
#    file_handler = FileHandler('error.log')
#    file_handler.setFormatter(
#        Formatter('%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]')
#    )
#    app.logger.setLevel(logging.INFO)
#    file_handler.setLevel(logging.INFO)
#    app.logger.addHandler(file_handler)
#    app.logger.info('errors')

#----------------------------------------------------------------------------#
# Launch.
#----------------------------------------------------------------------------#


if __name__ == '__main__':
    app.run(
        debug=app.config['DEBUG'],
        host=app.config['HOST'],
        port=app.config['PORT'],
        )
