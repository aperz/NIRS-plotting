import os
import pandas as pd
import io

def get_files_ext(dir, ext):
    return [f for f in os.listdir(dir) if f.endswith(ext)]


def parse_hdr(hdr_path):
    #TODO: there are also vectors:
    # ChanDis="30.0   30.0    30.0
    parsed = {}
    tables = []
    with open(hdr_path, 'r') as f:
        current = ''
        for l in f.readlines():
            l = l.strip()
            if l.startswith('[') and l.endswith(']'):
                current = l.strip('[]')
                parsed[current] = {}
            else:
                #TODO replace with re, something more specific
                if l.find('=') > -1:
                    current_ = l.split('=')[0]
                    parsed[current][current_] = []
                    value = l.split('=')[1]
                    parsed[current][current_].append(value)

                    if value.startswith('"#'):
                        tables.append({'k1':current, 'k2':current_})
                else:
                    value = l
                    parsed[current][current_].append(value)
    for t in tables:
        d = [row for row in parsed[t['k1']][t['k2']] if row not in ['', '"#', '#"']]
        d = '\n'.join(d)
        if not len(d) == 0:
            d = pd.read_csv(io.StringIO(d), sep='\t', header=None)

        parsed[t['k1']][t['k2']] = d
    return parsed

