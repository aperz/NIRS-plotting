from instance import instance
import os

config = dict(
        ROOT_DIR = os.path.abspath(os.path.dirname(__file__)),
        #app_dir = Flask.root_path,
        SECRET_KEY = 'secret!',
        )

config.update(
        DATA_DIR = os.path.join(config['ROOT_DIR'],'data')
        #STORAGE_DIR = 'storage',
        #UPLOADS_DIR = 'uploads',
    )

if instance.ISDEV:
    config.update(
        TITLE   = "nirs",
        PORT    = 2001,
        HOST    = 'localhost',
        VERBOSE = True,
        DEBUG   = True,
        )
else:
    config.update(
        TITLE   = "nirs",
        PORT    = 5000,
        # has to be 5000 for AWS
        HOST    = '0.0.0.0',
        VERBOSE = False,
        DEBUG   = False,
        )

#TODO for d in dirnames.values():
for d in ['DATA_DIR']:
    os.makedirs(config[d], exist_ok=True)

