#import matplotlib
#matplotlib.use('Agg')
import os
from flask import Flask, render_template, request, session, send_file
import pandas as pd
import bokeh.embed
#from forms import *
import nirs


from config import config
app = Flask(__name__)
app.config.update(config)


default_dataset = '2018-02-16_006'
default_channel = 0
default_wavelength = 1

datasets = os.listdir(app.config['DATA_DIR'])
wavelengths = [1,2]


@app.route('/')
def index():
    current_channel = request.args.get("channel_number")
    current_dataset = request.args.get("dataset_name")

    if current_channel == None:
        current_channel = default_channel
    else:
        try:
            current_channel = int(current_channel)
        except TypeError:
            current_channel = default_channel

    if current_dataset== None:
        current_dataset = default_dataset
    else:
        try:
            current_dataset = str(current_dataset)
        except TypeError:
            current_dataset = default_dataset

    #current_wavelength   = int(request.args.get("wavelength_number"))
    #if current_wavelength == None:
    #    current_wavelength = default_wavelength

    ds = nirs.Dataset(data_path = app.config['DATA_DIR'], experiment_name = current_dataset)
    #FIXME
    channels = ds.wl1.columns.values.tolist()
    #timepoints = ds.index.tolist()

    #plot_selection={1:[0,1,2,3], 2:[0,1,2,3]}
    #plot_selection = {current_wavelength: [current_channel]}
    plot_selection = {1: [current_channel], 2:[current_channel]}

    plot = ds.plot_bokeh_channels(plot_selection)

    script, div = bokeh.embed.components(plot)
    #table = ds.wl1.ix[:10,:10].to_html()
    #script,div,table = (str(current_channel), "", "")

    ###
    # plots can be a single Bokeh Model, a list/tuple, or even a dictionary
    #plots = {'Red': p1, 'Blue': p2, 'Green': p3}
    #script, div = components(plots)
    ###

    # DEMO plot
    plot_selection_demo={1:[0,1,2,3], 2:[0,1,2,3]}
    plot_demo = ds.plot_bokeh_channels(plot_selection_demo)
    script_demo, div_demo = bokeh.embed.components(plot_demo)

    return render_template("pages/plot_channels.html", script=script, div=div,
        channels=channels, datasets = datasets, wavelengths = wavelengths,
        #timepoints = timepoints,
        #start_timepoint, end_timepoint,
        current_channel=current_channel,
        current_dataset=current_dataset,
        script_demo = script_demo, div_demo=div_demo,
        )#, table=table)

