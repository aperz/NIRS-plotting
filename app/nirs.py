'''
TODO
[ ] plot selected channels on one plot
[ ] add a second plot underneath
[x] chose the dataset
[ ] timepoint: start,end

[ ] collapsing of channels by specified clusters
[ ] automatic choice of n clusters ('bundles')
'''


import seaborn as sns
import matplotlib.pyplot as plt
import bokeh.plotting
import util
import os
import pandas as pd
import numpy as np

class Dataset(object):
    def __init__(self, data_path, experiment_name):
        self.experiment_path = os.path.join(data_path, experiment_name)
        self.experiment_name = experiment_name

        # read in the data
        wl1_path = os.path.join(self.experiment_path,
            util.get_files_ext(self.experiment_path, "wl1")[0])
        self.wl1 = pd.read_table(wl1_path, sep=' ', header=None)
        wl2_path = os.path.join(self.experiment_path,
            util.get_files_ext(self.experiment_path, "wl2")[0])
        self.wl2 = pd.read_table(wl2_path, sep=' ', header=None)

        # read in the metadata
        hdr_path = os.path.join(self.experiment_path,
            util.get_files_ext(self.experiment_path, "hdr")[0])
        self.metadata = util.parse_hdr(hdr_path)

    def plot_test(self):
        plt.clf()
        plt.plot(wl1.iloc[:,:5])

    def plot_test_bokeh(self, channel=0):
        if not isinstance(channel, int):
            try:
                channel = int(channel)
            except:
                raise ValueError("'channel' must be of type int, instead is"+str(type(channel))+ str(channel))
        if not channel in self.wl1.columns:
            raise ValueError("'channel' was not found in dataset columns")


        df = self.wl1[channel]
        p = bokeh.plotting.figure(width=800, height=400)
        p.line( df.index, df,
                #title="Test plot. Showing channel "+str(channel),
                color='navy',
                #bins=bins,
                #legend='top_right',
                )
        #p.xaxis.axis_label = current_feature_name
        #p.yaxis.axis_label = 'Count'
        return p

    def plot_raw(self, data):
        plt.clf()
        plt.plot(data)

    def plot_bokeh_channels(self, channels={1:[0,1,2,3], 2:[0,1,2,3]}):
        #tools="pan,wheel_zoom,box_zoom,reset,save"
        tooltips=[('value', '$y'), ('timepoint', '$x')]

        for k,v in channels.items():
            if not k in [1,2]:
                raise ValueError("Wavelenght should be either 1 or 2 (int)")
            if not all([isinstance(c, int) for c in v]):
                try:
                    channels[k] = [int(c) for c in channels]
                except:
                    raise ValueError("'channels' must be a list of type int objects")
        if not all(c in self.wl1.columns for c in channels):
            raise ValueError("Not all channels were found in dataset columns")

        p = bokeh.plotting.figure(width=1000, height=400, tooltips=tooltips)

        for w in channels.keys():
            if w == 1:
                color = 'navy'
                df = self.wl1
            if w == 2:
                color = 'red'
                df = self.wl2

            for c in channels[w]:
                p.line( df[c].index, df[c],
                        #title="Test plot. Showing channel "+channel,
                        color=color,
                        #bins=bins,
                        legend=str("WL "+str(w)),
                        )
                #p.xaxis.axis_label = current_feature_name
                #p.yaxis.axis_label = 'Count'

        return p



# WORKIGN ON THIS HERE - is copied from Internet, need to change it
#def create_figure(current_feature_name, bins):
#    p = Histogram(iris_df, current_feature_name, title=current_feature_name, color='Species',
#            bins=bins, legend='top_right', width=600, height=400)
#
#    # Set the x axis label
#    p.xaxis.axis_label = current_feature_name
#
#    # Set the y axis label
#    p.yaxis.axis_label = 'Count'
#    return p
